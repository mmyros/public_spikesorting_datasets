============================
public_spikesorting_datasets
============================


.. image:: https://img.shields.io/pypi/v/public_spikesorting_datasets.svg
        :target: https://pypi.python.org/pypi/public_spikesorting_datasets

.. image:: https://img.shields.io/travis/mmyros/public_spikesorting_datasets.svg
        :target: https://travis-ci.com/mmyros/public_spikesorting_datasets

.. image:: https://readthedocs.org/projects/public-spikesorting-datasets/badge/?version=latest
        :target: https://public-spikesorting-datasets.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status


.. image:: https://pyup.io/repos/github/mmyros/public_spikesorting_datasets/shield.svg
     :target: https://pyup.io/repos/github/mmyros/public_spikesorting_datasets/
     :alt: Updates



Public spikesorted neuroscience datasets to test cluster quality


* Free software: MIT license
* Documentation: https://public-spikesorting-datasets.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
