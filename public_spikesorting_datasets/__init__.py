"""Top-level package for public_spikesorting_datasets."""

__author__ = """Maxym Myroshnychenko"""
__email__ = 'mmyros@gmail.com'
__version__ = '0.1.0'
